import webpack from 'webpack';
import dotenv from 'dotenv';
import paths from './paths';

dotenv.config();

const config: webpack.Configuration = {
  entry: paths.entry,
  module: {
    rules: [
      {
        test: /\.[jt]sx?$/,
        loader: 'esbuild-loader',
        options: {
          target: 'esnext',
        },
      },
      {
        test: /\.css$/,
        use: ['style-loader', 'css-loader'],
      },
      {
        test: /\.(png|jpe?g|gif)$/i,
        loader: 'file-loader',
        options: {
          name: 'img/[name].[contenthash].[ext]',
        },
      },
      {
        test: /\.svg$/,
        use: ['@svgr/webpack'],
      },
      {
        test: /\.mp3$/,
        loader: 'file-loader',
        options: {
          name: 'sound/[name].[contenthash].[ext]',
        },
      },
    ],
  },
  optimization: {
    splitChunks: false,
  },
  plugins: [
    // new webpack.container.ModuleFederationPlugin({
    //   name: '${REPO_NAME}',
    //   exposes: {
    //     '/app': '@app',
    //   },
    // }),
  ],
  resolve: {
    extensions: ['.tsx', '.ts', '.js', '.jsx', '.mjs'],
    alias: paths,
  },
  target: 'web',
};

export default config;
